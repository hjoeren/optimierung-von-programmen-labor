#include "measurement.h"

#include <cstddef>                            // for std::size_t
#include <cstdio>                             // for printf
#include <cstdlib>                            // for exit, atoi

#include <getopt.h>                           // for getopt

void show_help(const char* program_name) {
  printf("usage: %s [-option <argument>]\r\n", program_name);
  printf("option:\r\n");
  printf("    -? or -h:         show this help\r\n");
  printf("    -a <algorithm>:   run one or more special algorithm(s)\r\n");
  printf("                      unknown algorithms are ignored\r\n");
  printf("                      (default all algorithms)\r\n");
  printf("    -f:               write output to separate files\r\n");
  printf("                      (default false\r\n");
  printf("    -r <times>:       run the algorithm(s) multiple times (1-5)\r\n");
  printf("                      (default 1 time)\r\n");
  printf("algorithm:\r\n");
  printf("     minimum_search1:  backward minimum search\r\n");
  printf("     minimum_search2:  forward minimum search\r\n");
  printf("     selection_sort1:  selection sort with backward minimum\r\n");
  printf("                       search\r\n");
  printf("     selection_sort2:  selection sort with forward minimum\r\n");
  printf("                       search\r\n");
  printf("     insertion_sort1:  insertion sort without regarding special\r\n");
  printf("                       case\r\n");
  printf("     insertion_sort2:  insertion sort with regarding special\r\n");
  printf("                       case\r\n");
  printf("     merge_sort1:      bottom-up merge sort\r\n");
  printf("     merge_sort2:      natural merge sort\r\n");
  printf("     quick_sort1:      three-way partitioning quick sort\r\n");
  printf("     quick_sort2:      hybrid three-way partitioning quick sort\r\n");
}

int main(int argc, char** argv) {
  std::size_t number_of_algorithms = 0;
  const char** algorithms = new const char*[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

  std::size_t times = 1;

  bool file_flag = false;

  char option;
  while ((option = getopt(argc, argv, "h?a:fr:")) != -1) {
    switch (option) {
      case 'h': {
        show_help(argv[0]);
        exit(0);
      }
      case '?': {
        show_help(argv[0]);
        exit(0);
      }
      case 'a': {
        algorithms[number_of_algorithms] = optarg;
        number_of_algorithms++;
        break;
      }
      case 'f': {
        file_flag = true;
        break;
      }
      case 'r': {
        times = atoi(optarg);
        if (times < 1 || times > 5) {
          show_help(argv[0]);
          exit(-1);
        }
        break;
      }
      default: {
        show_help(argv[0]);
      }
    }
  }

  if (number_of_algorithms == 0) {
    algorithms[0] = "minimum_search1";
    algorithms[1] = "minimum_search2";
    algorithms[2] = "selection_sort1";
    algorithms[3] = "selection_sort2";
    algorithms[4] = "insertion_sort1";
    algorithms[5] = "insertion_sort2";
    algorithms[6] = "merge_sort1";
    algorithms[7] = "merge_sort2";
    algorithms[8] = "quick_sort1";
    algorithms[9] = "quick_sort2";
  }

  Measurement measurement(algorithms, times, file_flag);
  measurement.run();

  exit(0);
}
