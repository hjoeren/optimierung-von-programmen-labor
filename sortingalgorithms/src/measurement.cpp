#include "measurement.h"

#include "util.h"

template<std::size_t N>
void Measurement::prepare_run(std::array<double, N>& array,
                              std::size_t sort_order) {
  if (sort_order == 0) {
    initialize_ascending_array(array);
  } else if (sort_order == 1) {
    initialize_descending_array(array);
  } else if (sort_order == 2) {
    initialize_random_array(array);
  }
  reset_cache();
}

template<std::size_t N>
void Measurement::run(double& (*algorithm)(std::array<double, N>& array)) {
  if (algorithm == 0) {
    return;
  }

  fprintf(stream, "%d", N);
  fflush(stream);
  for (std::size_t i = 0; i < 3; i++) {
    long long sum_in_ms = 0;
    for (std::size_t j = 0; j < times; j++) {
      std::unique_ptr<std::array<double, N>> array(new std::array<double, N>());
      prepare_run(*array, i);
      auto start_time = std::chrono::high_resolution_clock::now();
      algorithm(*array);
      auto end_time = std::chrono::high_resolution_clock::now();
      long duration_in_ms =
          std::chrono::duration_cast<std::chrono::milliseconds>(
              end_time - start_time).count();
      fprintf(stream, ";%ld", duration_in_ms);
      fflush(stream);
      sum_in_ms += duration_in_ms;
    }
    if (times > 1) {
      double average_in_ms = (double) sum_in_ms / times;
      fprintf(stream, ";%.3f", average_in_ms);
      fflush(stream);
    }
  }
  fprintf(stream, "\n");
  fflush(stream);
}

template<std::size_t N>
void Measurement::run(void (*algorithm)(std::array<double, N>& array)) {
  if (algorithm == 0) {
    return;
  }

  fprintf(stream, "%d", N);
  fflush(stream);
  for (std::size_t i = 0; i < 3; i++) {
    long long sum_in_ms = 0;
    for (std::size_t j = 0; j < times; j++) {
      std::unique_ptr<std::array<double, N>> array(new std::array<double, N>());
      prepare_run(*array, i);
      auto start_time = std::chrono::high_resolution_clock::now();
      algorithm(*array);
      auto end_time = std::chrono::high_resolution_clock::now();
      long duration_in_ms =
          std::chrono::duration_cast<std::chrono::milliseconds>(
              end_time - start_time).count();
      fprintf(stream, ";%ld", duration_in_ms);
      fflush(stream);
      sum_in_ms += duration_in_ms;
    }
    if (times > 1) {
      double average_in_ms = (double) sum_in_ms / times;
      fprintf(stream, ";%.3f", average_in_ms);
      fflush(stream);
    }
  }
  fprintf(stream, "\n");
}

Measurement::Measurement(const char** algorithms, std::size_t times,
                         bool file_flag) {
  this->available_algorithms = new const char*[10];
  this->available_algorithms[MINIMUM_SEARCH1] = "minimum_search1";
  this->available_algorithms[MINIMUM_SEARCH2] = "minimum_search2";
  this->available_algorithms[SELECTION_SORT1] = "selection_sort1";
  this->available_algorithms[SELECTION_SORT2] = "selection_sort2";
  this->available_algorithms[INSERTION_SORT1] = "insertion_sort1";
  this->available_algorithms[INSERTION_SORT2] = "insertion_sort2";
  this->available_algorithms[MERGE_SORT1] = "merge_sort1";
  this->available_algorithms[MERGE_SORT2] = "merge_sort2";
  this->available_algorithms[QUICK_SORT1] = "quick_sort1";
  this->available_algorithms[QUICK_SORT2] = "quick_sort2";

  this->algorithms = algorithms;
  this->times = times;

  if (file_flag == false) {
    stream = stdout;
  }
}

void Measurement::run() {
  for (std::size_t i = 0; algorithms[i] != 0 && i < 10; i++) {
    bool minimum_search = false;
    bool sort = false;

    double& (*minimum_search_level00)(std::array<double, 16000>&) = 0;
    double& (*minimum_search_level01)(std::array<double, 32000>&) = 0;
    double& (*minimum_search_level02)(std::array<double, 64000>&) = 0;
    double& (*minimum_search_level03)(std::array<double, 128000>&) = 0;
    double& (*minimum_search_level04)(std::array<double, 256000>&) = 0;
    double& (*minimum_search_level05)(std::array<double, 512000>&) = 0;
    double& (*minimum_search_level06)(std::array<double, 1024000>&) = 0;
    double& (*minimum_search_level07)(std::array<double, 2048000>&) = 0;
    double& (*minimum_search_level08)(std::array<double, 4096000>&) = 0;
    double& (*minimum_search_level09)(std::array<double, 8192000>&) = 0;

    void (*sort_level00)(std::array<double, 16000>&) = 0;
    void (*sort_level01)(std::array<double, 32000>&) = 0;
    void (*sort_level02)(std::array<double, 64000>&) = 0;
    void (*sort_level03)(std::array<double, 128000>&) = 0;
    void (*sort_level04)(std::array<double, 256000>&) = 0;
    void (*sort_level05)(std::array<double, 512000>&) = 0;
    void (*sort_level06)(std::array<double, 1024000>&) = 0;
    void (*sort_level07)(std::array<double, 2048000>&) = 0;
    void (*sort_level08)(std::array<double, 4096000>&) = 0;
    void (*sort_level09)(std::array<double, 8192000>&) = 0;
    void (*sort_level10)(std::array<double, 16384000>&) = 0;
    void (*sort_level11)(std::array<double, 32768000>&) = 0;
    void (*sort_level12)(std::array<double, 65536000>&) = 0;

    if (strcmp(algorithms[i], available_algorithms[MINIMUM_SEARCH1]) == 0) {
      minimum_search = true;

      minimum_search_level00 = &search_minimum1;
      minimum_search_level01 = &search_minimum1;
      minimum_search_level02 = &search_minimum1;
      minimum_search_level03 = &search_minimum1;
      minimum_search_level04 = &search_minimum1;
      minimum_search_level05 = &search_minimum1;
      minimum_search_level06 = &search_minimum1;
      minimum_search_level07 = &search_minimum1;
      minimum_search_level08 = &search_minimum1;
      minimum_search_level09 = &search_minimum1;
    } else if (strcmp(algorithms[i], available_algorithms[MINIMUM_SEARCH2])
        == 0) {
      minimum_search = true;

      minimum_search_level00 = &search_minimum2;
      minimum_search_level01 = &search_minimum2;
      minimum_search_level02 = &search_minimum2;
      minimum_search_level03 = &search_minimum2;
      minimum_search_level04 = &search_minimum2;
      minimum_search_level05 = &search_minimum2;
      minimum_search_level06 = &search_minimum2;
      minimum_search_level07 = &search_minimum2;
      minimum_search_level08 = &search_minimum2;
      minimum_search_level09 = &search_minimum2;
    } else if (strcmp(algorithms[i], available_algorithms[SELECTION_SORT1])
        == 0) {
      sort = true;

      sort_level00 = &sort_selection1;
      sort_level01 = &sort_selection1;
      sort_level02 = &sort_selection1;
      sort_level03 = &sort_selection1;
      sort_level04 = &sort_selection1;
      sort_level05 = &sort_selection1;
      sort_level06 = &sort_selection1;
      sort_level07 = &sort_selection1;
      sort_level08 = &sort_selection1;
      sort_level09 = &sort_selection1;
    } else if (strcmp(algorithms[i], available_algorithms[SELECTION_SORT2])
        == 0) {
      sort = true;

      sort_level00 = &sort_selection2;
      sort_level01 = &sort_selection2;
      sort_level02 = &sort_selection2;
      sort_level03 = &sort_selection2;
      sort_level04 = &sort_selection2;
      sort_level05 = &sort_selection2;
      sort_level06 = &sort_selection2;
      sort_level07 = &sort_selection2;
      sort_level08 = &sort_selection2;
      sort_level09 = &sort_selection2;
    } else if (strcmp(algorithms[i], available_algorithms[INSERTION_SORT1])
        == 0) {
      sort = true;

      sort_level00 = &sort_insertion1;
      sort_level01 = &sort_insertion1;
      sort_level02 = &sort_insertion1;
      sort_level03 = &sort_insertion1;
      sort_level04 = &sort_insertion1;
      sort_level05 = &sort_insertion1;
      sort_level06 = &sort_insertion1;
      sort_level07 = &sort_insertion1;
      sort_level08 = &sort_insertion1;
      sort_level09 = &sort_insertion1;
    } else if (strcmp(algorithms[i], available_algorithms[INSERTION_SORT2])
        == 0) {
      sort = true;

      sort_level00 = &sort_insertion2;
      sort_level01 = &sort_insertion2;
      sort_level02 = &sort_insertion2;
      sort_level03 = &sort_insertion2;
      sort_level04 = &sort_insertion2;
      sort_level05 = &sort_insertion2;
      sort_level06 = &sort_insertion2;
      sort_level07 = &sort_insertion2;
      sort_level08 = &sort_insertion2;
      sort_level09 = &sort_insertion2;
    } else if (strcmp(algorithms[i], available_algorithms[MERGE_SORT1]) == 0) {
      sort = true;

      sort_level00 = &sort_merge1;
      sort_level01 = &sort_merge1;
      sort_level02 = &sort_merge1;
      sort_level03 = &sort_merge1;
      sort_level04 = &sort_merge1;
      sort_level05 = &sort_merge1;
      sort_level06 = &sort_merge1;
      sort_level07 = &sort_merge1;
      sort_level08 = &sort_merge1;
      sort_level09 = &sort_merge1;
      sort_level10 = &sort_merge1;
      sort_level11 = &sort_merge1;
      sort_level12 = &sort_merge1;
    } else if (strcmp(algorithms[i], available_algorithms[MERGE_SORT2]) == 0) {
      sort = true;

      sort_level00 = &sort_merge2;
      sort_level01 = &sort_merge2;
      sort_level02 = &sort_merge2;
      sort_level03 = &sort_merge2;
      sort_level04 = &sort_merge2;
      sort_level05 = &sort_merge2;
      sort_level06 = &sort_merge2;
      sort_level07 = &sort_merge2;
      sort_level08 = &sort_merge2;
      sort_level09 = &sort_merge2;
      sort_level10 = &sort_merge2;
      sort_level11 = &sort_merge2;
      sort_level12 = &sort_merge2;
    } else if (strcmp(algorithms[i], available_algorithms[QUICK_SORT1]) == 0) {
      sort = true;

      sort_level00 = &sort_quick1;
      sort_level01 = &sort_quick1;
      sort_level02 = &sort_quick1;
      sort_level03 = &sort_quick1;
      sort_level04 = &sort_quick1;
      sort_level05 = &sort_quick1;
      sort_level06 = &sort_quick1;
      sort_level07 = &sort_quick1;
      sort_level08 = &sort_quick1;
      sort_level09 = &sort_quick1;
      sort_level10 = &sort_quick1;
      sort_level11 = &sort_quick1;
      sort_level12 = &sort_quick1;
    } else if (strcmp(algorithms[i], available_algorithms[QUICK_SORT2]) == 0) {
      sort = true;

      sort_level00 = &sort_quick2;
      sort_level01 = &sort_quick2;
      sort_level02 = &sort_quick2;
      sort_level03 = &sort_quick2;
      sort_level04 = &sort_quick2;
      sort_level05 = &sort_quick2;
      sort_level06 = &sort_quick2;
      sort_level07 = &sort_quick2;
      sort_level08 = &sort_quick2;
      sort_level09 = &sort_quick2;
      sort_level10 = &sort_quick2;
      sort_level11 = &sort_quick2;
      sort_level12 = &sort_quick2;
    }

    if (minimum_search == false && sort == false) {
      continue;
    }

    if (stream != stdout) {
      char file_name[20];
      strcpy(file_name, algorithms[i]);
      strcat(file_name, ".csv\0");
      stream = fopen(file_name, "w");
    } else {
      printf("##########################\r\n");
      printf("###%*s%*s###\r\n", 10 + strlen(algorithms[i]) / 2, algorithms[i],
             10 - strlen(algorithms[i]) / 2, "");
      printf("##########################\r\n");
    }

    if (minimum_search == true) {
      run(minimum_search_level00);
      run(minimum_search_level01);
      run(minimum_search_level02);
      run(minimum_search_level03);
      run(minimum_search_level04);
      run(minimum_search_level05);
      run(minimum_search_level06);
      run(minimum_search_level07);
      run(minimum_search_level08);
      run(minimum_search_level09);
    } else {
      run(sort_level00);
      run(sort_level01);
      run(sort_level02);
      run(sort_level03);
      run(sort_level04);
      run(sort_level05);
      run(sort_level06);
      run(sort_level07);
      run(sort_level08);
      run(sort_level09);
      run(sort_level10);
      run(sort_level11);
      run(sort_level12);
    }

    if (stream != stdout) {
      fclose(stream);
    } else {
      printf("\r\n");
      printf("\r\n");
    }
  }

}
