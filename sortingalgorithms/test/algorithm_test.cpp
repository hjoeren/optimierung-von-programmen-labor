#include "algorithm_test.h"

#include "util.h"

// Individual tests
TEST(AlgorithmTest, MinimumSearch1WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  int expected_minimum = 5;
  int found_minimum = search_minimum1(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch1WithOnlyOneElement_Double) {
  std::array<double, 1> array( { 12.0 });
  double expected_minimum = 12.0;
  double found_minimum = search_minimum1(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch1WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "y" });
  std::string expected_minimum = "y";
  std::string found_minimum = search_minimum1(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch1WithRandomValues_Int) {
  std::array<int, 10> array( { 30, 15, 49, 46, 28, 9, 10, 26, 36, 15 });
  int expected_minimum = 9;
  int found_minimum = search_minimum1(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch1WithRandomValues_Double) {
  std::array<double, 10> array( { 30.5, 15.2, 49.3, 46.54, 28.12, 9.12, 10.56,
      26.12, 36.98, 15.10 });
  double expected_minimum = 9.12;
  double found_minimum = search_minimum1(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch1WithRandomValues_String) {
  std::array<std::string, 10> array( { "a", "f", "a", "i", "g", "G", "y", "A",
      "i", "Z" });
  std::string expected_minimum = "A";
  std::string found_minimum = search_minimum1(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch2WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  int expected_minimum = 5;
  int found_minimum = search_minimum2(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch2WithOnlyOneElement_Double) {
  std::array<double, 1> array( { 12.0 });
  double expected_minimum = 12.0;
  double found_minimum = search_minimum2(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch2WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "y" });
  std::string expected_minimum = "y";
  std::string found_minimum = search_minimum2(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch2WithRandomValues_Int) {
  std::array<int, 10> array( { 30, 15, 49, 46, 28, 9, 10, 26, 36, 15 });
  int expected_minimum = 9;
  int found_minimum = search_minimum2(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch2WithRandomValues_Double) {
  std::array<double, 10> array( { 30.5, 15.2, 49.3, 46.54, 28.12, 9.12, 10.56,
      26.12, 36.98, 15.10 });
  double expected_minimum = 9.12;
  double found_minimum = search_minimum2(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, MinimumSearch2WithRandomValues_String) {
  std::array<std::string, 10> array( { "a", "f", "a", "i", "g", "G", "y", "A",
      "i", "Z" });
  std::string expected_minimum = "A";
  std::string found_minimum = search_minimum2(array);
  ASSERT_EQ(expected_minimum, found_minimum);
}

TEST(AlgorithmTest, SelectionSort1WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort1WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort1WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort1WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort1WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort1WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort1WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort2WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_selection2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort2WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_selection2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort2WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_selection2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort2WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_selection2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort2WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_selection2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort2WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_selection1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, SelectionSort2WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_selection2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort1WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_insertion1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort1WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_insertion1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort1WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_insertion1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort1WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_insertion1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort1WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_insertion1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort1WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_insertion1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort1WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_insertion1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort2WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_insertion2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort2WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_insertion2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort2WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_insertion2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort2WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_insertion2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort2WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_insertion2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort2WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_insertion2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, InsertionSort2WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_insertion2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort1WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_merge1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort1WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_merge1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort1WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_merge1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort1WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_merge1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort1WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_merge1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort1WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_merge1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort1WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_merge1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort2WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_merge2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort2WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_merge2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort2WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_merge2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort2WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_merge2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort2WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_merge2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort2WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_merge2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, MergeSort2WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_merge2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort1WithManyValues_int) {
  std::array<int, 400> array;
  std::array<int, 400> expected_array;
  for (int i = 0; i < 400; i++) {
    array[i] = i;
    expected_array[i] = i;
  }
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::shuffle(array.begin(), array.end(), std::default_random_engine(seed));
  sort_quick1(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithOnlyOneElement_Int) {
  std::array<int, 1> array( { 5 });
  std::array<int, 1> expected_array( { 5 });
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithOnlyOneElement_double) {
  std::array<double, 1> array( { 12.85 });
  std::array<double, 1> expected_array( { 12.85 });
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithOnlyOneElement_String) {
  std::array<std::string, 1> array( { "hallo" });
  std::array<std::string, 1> expected_array( { "hallo" });
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithRandomValues_Int) {
  std::array<int, 10> array( { 31, 21, 44, 12, 3, 4, 27, 29, 3, 42 });
  std::array<int, 10> expected_array( { 3, 3, 4, 12, 21, 27, 29, 31, 42, 44 });
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithRandomValues_Double) {
  std::array<double, 10> array( { 31.1, 21.2, 44.3, 12.4, 3.5, 4.6, 27.7, 29.8,
      3.5, 42.10 });
  std::array<double, 10> expected_array( { 3.5, 3.5, 4.6, 12.4, 21.2, 27.7,
      29.8, 31.1, 42.10, 44.3 });
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithRandomValues_String) {
  std::array<std::string, 12> array( { "h", "s", "-", "k", "a", "r", "l", "s",
      "r", "u", "h", "e" });
  std::array<std::string, 12> expected_array( { "-", "a", "e", "h", "h", "k",
      "l", "r", "r", "s", "s", "u" });
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithDoubleValues_Int) {
  std::array<int, 10> array({-9, 8, -7, 6, -5, -5, 6, -7, 8, -9});
  std::array<int, 10> expected_array({-9, -9, -7, -7, -5, -5, 6, 6, 8, 8});
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

TEST(AlgorithmTest, QuickSort2WithManyValues_int) {
  std::array<int, 400> array;
  std::array<int, 400> expected_array;
  for (int i = 0; i < 400; i++) {
    array[i] = i;
    expected_array[i] = i;
  }
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::shuffle(array.begin(), array.end(), std::default_random_engine(seed));
  sort_quick2(array);
  ASSERT_EQ(expected_array, array);
}

template<class T>
void AlgorithmTest<T>::SetUp() {
  random_array = std::array<T, ARRAY_SIZE>();
  mirrored_array = std::array<T, ARRAY_SIZE>();

  initialize_random_array(random_array);
  initialize_mirrored_array(mirrored_array);

  expected_minimum_of_random_array = *std::min_element(random_array.begin(),
                                                       random_array.end());
  expected_minimum_of_mirrored_array = *std::min_element(mirrored_array.begin(),
                                                         mirrored_array.end());

  expected_array_of_random_array = std::array<T, ARRAY_SIZE>(random_array);
  std::sort(expected_array_of_random_array.begin(),
            expected_array_of_random_array.end());
  expected_array_of_mirrored_array = std::array<T, ARRAY_SIZE>(mirrored_array);
  std::sort(expected_array_of_mirrored_array.begin(),
            expected_array_of_mirrored_array.end());
}
/*
typedef ::testing::Types<int, long, float, double, std::string> types;
TYPED_TEST_CASE(AlgorithmTest, types);

TYPED_TEST(AlgorithmTest, MinimumSearch1Random){
this->resulted_minimum = search_minimum1(this->random_array);
ASSERT_EQ(this->expected_minimum_of_random_array, this->resulted_minimum);
}

TYPED_TEST(AlgorithmTest, MinimumSearch1Mirrored){
this->resulted_minimum = search_minimum1(this->mirrored_array);
ASSERT_EQ(this->expected_minimum_of_mirrored_array, this->resulted_minimum);
}

TYPED_TEST(AlgorithmTest, MinimumSearch2Random){
this->resulted_minimum = search_minimum2(this->random_array);
ASSERT_EQ(this->expected_minimum_of_random_array, this->resulted_minimum);
}

TYPED_TEST(AlgorithmTest, MinimumSearch2Mirrored){
this->resulted_minimum = search_minimum2(this->mirrored_array);
ASSERT_EQ(this->expected_minimum_of_mirrored_array, this->resulted_minimum);
}

TYPED_TEST(AlgorithmTest, SelectionSort1Random){
sort_selection1(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, SelectionSort1Mirrored){
sort_selection1(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}

TYPED_TEST(AlgorithmTest, SelectionSort2Random){
sort_selection2(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, SelectionSort2Mirrored){
sort_selection2(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}

TYPED_TEST(AlgorithmTest, InsertionSort1Random){
sort_insertion1(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, InsertionSort1Mirrored){
sort_insertion1(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}

TYPED_TEST(AlgorithmTest, InsertionSort2Random){
sort_insertion2(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, InsertionSort2Mirrored){
sort_insertion2(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}

TYPED_TEST(AlgorithmTest, MergeSort1Random){
sort_merge1(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, MergeSort1Mirrored){
sort_merge1(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}

TYPED_TEST(AlgorithmTest, MergeSort2Random){
sort_merge2(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, MergeSort2Mirrored){
sort_merge2(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}

TYPED_TEST(AlgorithmTest, QuickSort1Random){
sort_quick1(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, QuickSort1Mirrored){
sort_quick1(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}

TYPED_TEST(AlgorithmTest, QuickSort2Random){
sort_quick2(this->random_array);
ASSERT_EQ(this->expected_array_of_random_array, this->random_array);
}

TYPED_TEST(AlgorithmTest, QuickSort2Mirrored){
sort_quick2(this->mirrored_array);
ASSERT_EQ(this->expected_array_of_mirrored_array, this->mirrored_array);
}
*/
