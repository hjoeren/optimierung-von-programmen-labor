#ifndef INSERTION_SORT_H_
#define INSERTION_SORT_H_

#include "minimum_search.h"

#include <cstddef>                            // for std::size_t

#include <array>                              // for std::array
#include <utility>                            // for std::swap

template<class T, std::size_t N>
void sort_insertion1(std::array<T, N>& array, std::size_t left,
                     std::size_t right) {
  for (std::size_t i = left; i < right; i++) {
    T temp = array[i];
    for (std::size_t j = i; (j > left) && (temp < array[j - 1]); j--) {
      std::swap(array[j], array[j - 1]);
    }
  }
}

template<class T, std::size_t N>
void sort_insertion1(std::array<T, N>& array) {
  for (std::size_t i = 1; i < N; i++) {
    T temp = array[i];
    for (std::size_t j = i; (j > 0) && (temp < array[j - 1]); j--) {
      std::swap(array[j], array[j - 1]);
    }
  }
}

template<class T, std::size_t N>
void sort_insertion2(std::array<T, N>& array) {
  std::swap(array[0], search_minimum1(array));
  for (std::size_t i = 1; i < N; i++) {
    T temp = array[i];
    for (std::size_t j = i; (temp < array[j - 1]); j--) {
      std::swap(array[j], array[j - 1]);
    }
  }
}
#endif /* INSERTION_SORT_H_ */
