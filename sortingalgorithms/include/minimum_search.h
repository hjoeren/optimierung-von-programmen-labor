#ifndef MINIMUM_SEARCH_H_
#define MINIMUM_SEARCH_H_

#include <cstddef>                            // for std::size_t

#include <array>                              // for std::array

template<class T, std::size_t N>
T& search_minimum1(std::array<T, N>& array, std::size_t start_index) {
 std::size_t index_of_minimum = N - 1;
 for (std::ptrdiff_t i = N - 2; i >= (std::ptrdiff_t) start_index; i--) {
   if (array[i] <= array[index_of_minimum]) {
     index_of_minimum = i;
   }
 }
 return array[index_of_minimum];
}

template<class T, std::size_t N>
T& search_minimum1(std::array<T, N>& array) {
  return search_minimum1(array, 0);
}

template<class T, std::size_t N>
T& search_minimum2(std::array<T, N>& array, std::size_t start_index) {
  std::size_t index_of_minimum = start_index;
  for (std::size_t i = index_of_minimum + 1; i < N; i++) {
    if (array[i] < array[index_of_minimum]) {
      index_of_minimum = i;
    }
  }
  return array[index_of_minimum];
}

template<class T, std::size_t N>
T& search_minimum2(std::array<T, N>& array) {
  return search_minimum2(array, 0);
}

#endif /* MINIMUM_SEARCH_H_ */
