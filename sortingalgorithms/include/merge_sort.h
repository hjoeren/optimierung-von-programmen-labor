#ifndef MERGE_SORT_H_
#define MERGE_SORT_H_

#include <cstddef>                            // for std::size_t

#include <array>                              // for std::array
#include <memory>                             // for std::unique_ptr

template<class T, std::size_t N>
void merge1(std::array<T, N>& array, std::array<T, N>& aux_array,
            std::size_t left, std::size_t mid, std::size_t right) {
  std::size_t l = left;
  std::size_t r = mid + 1;

  for (std::size_t i = left; i <= right; i++) {
    if (l <= mid && r <= right) {
      if (array[l] <= array[r]) {
        aux_array[i] = array[l];
        l++;
      } else {
        aux_array[i] = array[r];
        r++;
      }
    } else if (l > mid) {
      aux_array[i] = array[r];
      r++;
    } else if (r > right) {
      aux_array[i] = array[l];
      l++;
    }
  }

  for (std::size_t i = left; i <= right; i++) {
    array[i] = aux_array[i];
  }
}

template<class T, std::size_t N>
void sort_merge1(std::array<T, N>& array) {
  std::unique_ptr<std::array<T, N>> aux_array_ptr(new std::array<T, N>(array));

  for (std::size_t width = 1; width < N; width *= 2) {
    for (std::size_t left = 0; left < N; left += 2 * width) {
      std::size_t mid = left + width - 1;
      std::size_t right;

      if (left + 2 * width - 1 < N) {
        right = left + 2 * width - 1;
      } else {
        right = N - 1;
      }

      merge1(array, *aux_array_ptr, left, mid, right);
    }
  }
}

template<class T, std::size_t N>
void merge2(std::array<T, N>& array, std::array<T, N>& aux_array,
            std::size_t left, std::size_t right, int direction) {
  std::size_t l = left;
  std::size_t r = right;

  for (std::size_t i = (direction == 1) ? left : right; l <= r; i +=
      direction) {
    if (array[l] <= array[r]) {
      aux_array[i] = array[l];
      l++;
    } else {
      aux_array[i] = array[r];
      r--;
    }
  }

  for (std::size_t i = left; i <= right; i++) {
    array[i] = aux_array[i];
  }
}

template<class T, std::size_t N>
void sort_merge2(std::array<T, N>& array) {
  std::unique_ptr<std::array<T, N>> aux_array(new std::array<T, N>());

  std::size_t right;
  std::size_t left = 1;
  int direction;

  T temp;

  while (left != 0) {
    right = 0;
    left = 0;
    direction = 1;

    while (right < N) {
      left = right;
      temp = array[right];
      right++;

      while (right < N && temp <= array[right]) {
        temp = array[right];
        right++;
      }
      while (right < N && temp >= array[right]) {
        temp = array[right];
        right++;
      }

      merge2(array, *aux_array, left, right - 1, direction);
      direction *= -1;
    }
  }
}

#endif /* MERGE_SORT_H_ */
