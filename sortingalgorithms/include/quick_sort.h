#ifndef QUICK_SORT_H_
#define QUICK_SORT_H_

#include <cstddef>                            // for std::size_t, std::ptrdiff_t

#include <array>                              // for std::array
#include <utility>                            // for std::swap

#include "insertion_sort.h"
#include "merge_sort.h"

#define FACTOR 10
#define MINIMUM 25

template<class T, std::size_t N>
void sort_quick1(std::array<T, N>& array, std::ptrdiff_t left,
                 std::ptrdiff_t right) {
  if (right <= left) {
    return;
  }

  std::swap(array[(right + left) / 2], array[right]);
  T pivot = array[right];

  std::size_t i = left - 1;
  std::size_t p = left - 1;
  std::size_t j = right;
  std::size_t q = right;
  std::size_t k;

  while (true) {
    while (array[++i] < pivot) {
    }
    while (array[--j] > pivot) {
      if (j == left) {
        break;
      }
    }

    if (i >= j) {
      break;
    }

    std::swap(array[i], array[j]);

    if (array[i] == pivot) {
      p++;
      std::swap(array[p], array[i]);
    }
    if (array[j] == pivot) {
      q--;
      std::swap(array[q], array[j]);
    }
  }

  std::swap(array[i], array[right]);
  j = i - 1;
  i = i + 1;
  for (k = left; k <= p && p <= N; k++, j--) {
    std::swap(array[k], array[j]);
  }
  for (k = right - 1; k >= q && k <= N; k--, i++) {
    std::swap(array[k], array[i]);
  }

  sort_quick1(array, left, j);
  sort_quick1(array, i, right);
}

template<class T, std::size_t N>
void sort_quick1(std::array<T, N>& array) {
  sort_quick1(array, 0, N - 1);
}

template<class T, std::size_t N>
void sort_quick2(std::array<T, N>& array, std::array<T, N>& aux_array,
                 std::ptrdiff_t left, std::ptrdiff_t right) {
  if (right <= left) {
    return;
  }

  if ((right - left) < MINIMUM) {
    sort_insertion1(array, left, right + 1);
  } else {

    std::swap(array[(right + left) / 2], array[right]);
    T pivot = array[right];

    std::size_t i = left - 1;
    std::size_t p = left - 1;
    std::size_t j = right;
    std::size_t q = right;
    std::size_t k;

    while (true) {
      while (array[++i] < pivot) {
      }
      while (array[--j] > pivot) {
        if (j == left) {
          break;
        }
      }
      if (i >= j) {
        break;
      }
      std::swap(array[i], array[j]);
      if (array[i] == pivot) {
        p++;
        std::swap(array[p], array[i]);
      }
      if (array[j] == pivot) {
        q--;
        std::swap(array[q], array[j]);
      }
    }
    std::swap(array[i], array[right]);
    j = i - 1;
    i = i + 1;
    for (k = left; k <= p && p <= N; k++, j--) {
      std::swap(array[k], array[j]);
    }
    for (k = right - 1; k >= q && k <= N; k--, i++) {
      std::swap(array[k], array[i]);
    }

    std::size_t deltaJ = j - left;
    std::size_t deltaI = right - i;
    if ((deltaJ * FACTOR) < deltaI || (deltaI * FACTOR) < deltaJ) {
      std::size_t m = (left + right) / 2;
      sort_quick2(array, aux_array, left, m);
      sort_quick2(array, aux_array, m + 1, right);
      merge1(array, aux_array, left, m, right);
    } else {
      sort_quick2(array, aux_array, left, j);
      sort_quick2(array, aux_array, i, right);
    }
  }
}

template<class T, std::size_t N>
void sort_quick2(std::array<T, N>& array) {
  std::unique_ptr<std::array<T, N>> aux_array(new std::array<T, N>());

  sort_quick2(array, *aux_array, 0, N - 1);
}

#endif /* QUICK_SORT_H_ */
