#ifndef UTIL_H_
#define UTIL_H_

#include <cstddef>                            // for std::size_t
#include <cstdlib>                            // for std::malloc

#include <array>                              // for std::array
#include <chrono>                             // for std::chrono
#include <limits>                             // for std::numeric_limits
#include <random>                             // for std::uniform_int_distribution, std::uniform_real_distribution and std::mt19937_64
#include <sstream>                            // for std::stringstream
#include <string>                             // for std::string

template<class T>
std::string to_string(T value) {
  std::stringstream stream;
  stream << value;
  return stream.str();
}

template<class T, std::size_t N>
void initialize_ascending_array(std::array<T, N>& array) {
  for (std::size_t i = 0; i < N; i++) {
    array[i] = T(i);
  }
}

template<std::size_t N>
void initialize_ascending_array(std::array<std::string, N>& array) {
  for (std::size_t i = 0; i < N; i++) {
    array[i] = to_string(i);
  }
}

template<class T, std::size_t N>
void initialize_descending_array(std::array<T, N>& array) {
  for (std::size_t i = 0; i < N; i++) {
    array[i] = T((N - 1) - i);
  }
}

template<std::size_t N>
void initialize_descending_array(std::array<std::string, N>& array) {
  for (std::size_t i = 0; i < N; i++) {
    array[i] = to_string((N - 1) - i);
  }
}

template<class T, std::size_t N>
void initialize_random_array(std::array<T, N>& array) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::uniform_real_distribution<double> dist(
      std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
  std::mt19937_64 mt(seed);
  for (std::size_t i = 0; i < N; i++) {
    array[i] = T(dist(mt));
  }
}

template<std::size_t N>
void initialize_random_array(std::array<std::string, N>& array) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::uniform_int_distribution<char> dist(std::numeric_limits<char>::min(),
                                             std::numeric_limits<char>::max());
  std::mt19937_64 mt(seed);
  for (std::size_t i = 0; i < N; i++) {
    array[i] = to_string(dist(mt));
  }
}

template<class T, std::size_t N>
void initialize_mirrored_array(std::array<T, N>& array) {
  for (std::size_t i = 0; i < N; i++) {
    array[i] = T(i);
    array[(N - 1) - i] = T(i);
  }
}

template<std::size_t N>
void initialize_mirrored_array(std::array<std::string, N>& array) {
  for (std::size_t i = 0; i < N; i++) {
    array[i] = to_string(i);
    array[(N - 1) - i] = to_string(i);
  }
}

void reset_cache() {
  std::size_t size = 20000000;
  char* bytes = (char*) std::malloc(size);
  for (std::size_t i = 0; i < size; i++) {
    bytes[i] = 0;
  }
}

#endif /* UTIL_H_ */
