#ifndef MEASUREMENT_H_
#define MEASUREMENT_H_

#include "minimum_search.h"
#include "selection_sort.h"
#include "insertion_sort.h"
#include "merge_sort.h"
#include "quick_sort.h"

#include <cstddef>                            // for std::size_t
#include <cstdio>                             // for fprintf
#include <cstring>

#include <array>                              // for std::array
#include <chrono>                             // for std::chrono
#include <memory>                             // for std::unique_ptr

class Measurement {
 private:
  static const std::size_t MINIMUM_SEARCH1 = 0;
  static const std::size_t MINIMUM_SEARCH2 = 1;
  static const std::size_t SELECTION_SORT1 = 2;
  static const std::size_t SELECTION_SORT2 = 3;
  static const std::size_t INSERTION_SORT1 = 4;
  static const std::size_t INSERTION_SORT2 = 5;
  static const std::size_t MERGE_SORT1 = 6;
  static const std::size_t MERGE_SORT2 = 7;
  static const std::size_t QUICK_SORT1 = 8;
  static const std::size_t QUICK_SORT2 = 9;

  const char** available_algorithms;

  const char** algorithms;
  std::size_t times;

  FILE* stream;

  template<std::size_t N>
  void prepare_run(std::array<double, N>& array, std::size_t sort_order);

  template<std::size_t N>
  void run(double& (*algorithm)(std::array<double, N>& array));
  template<std::size_t N>
  void run(void (*algorithm)(std::array<double, N>& array));
 public:
  Measurement(const char** algorithms, std::size_t times, bool file_flag);

  void run();
};

#endif /* MEASUREMENT_H_ */
