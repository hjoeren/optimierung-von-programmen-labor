#ifndef SELECTION_SORT_H_
#define SELECTION_SORT_H_

#include "minimum_search.h"

#include <cstddef>                            // for std::size_t

#include <array>                              // for std::array
#include <utility>                            // for std::swap

template<class T, std::size_t N>
void sort_selection1(std::array<T, N>& array) {
  for (std::size_t i = 0; i < N - 1; i++) {
      std::swap(array[i], search_minimum1(array, i));
    }
}

template<class T, std::size_t N>
void sort_selection2(std::array<T, N>& array) {
  for (std::size_t i = 0; i < N - 1; i++) {
    std::swap(array[i], search_minimum2(array, i));
  }
}

#endif /* SELECTION_SORT_H_ */
