#ifndef ALGORITHM_TEST_H_
#define ALGORITHM_TEST_H_

#include "minimum_search.h"
#include "selection_sort.h"
#include "insertion_sort.h"
#include "merge_sort.h"
#include "quick_sort.h"

#include <cstddef>                            // for std::size_t

#include <algorithm>                          // for std::min_element, std::sort
#include <array>                              // for std::array

#include <gtest/gtest.h>                      // for ::testing

template<class T>
class AlgorithmTest : public ::testing::Test {
 public:
  static const std::size_t ARRAY_SIZE = 1;

  std::array<T, ARRAY_SIZE> random_array;
  std::array<T, ARRAY_SIZE> mirrored_array;

  T expected_minimum_of_random_array;
  T expected_minimum_of_mirrored_array;
  T resulted_minimum;

  std::array<T, ARRAY_SIZE> expected_array_of_random_array;
  std::array<T, ARRAY_SIZE> expected_array_of_mirrored_array;
  std::array<T, ARRAY_SIZE> resulted_array;

  void SetUp();
};

#endif /* ALGORITHM_TEST_H_ */
